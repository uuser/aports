# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-multidict
_pkgname=multidict
pkgver=4.7.0
pkgrel=0
pkgdesc="A multidict implementation"
url="https://github.com/aio-libs/multidict/"
arch="all"
license="Apache-2.0"
depends="python3"
makedepends="python3-dev py3-setuptools cython"
subpackages="$pkgname-dev"
source="$_pkgname-$pkgver.tar.gz::https://github.com/aio-libs/multidict/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!strip"

check() {
	python3 setup.py check
}

build() {
	(cd multidict && find -name '*.pyx' -exec cython {} \;)
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
	rm -f "$pkgdir"/usr/lib/python3*/site-packages/*.c
	rm -f "$pkgdir"/usr/lib/python3*/site-packages/*.h
}

sha512sums="c0b564db122c2fbb8c812e901c7ba84b59c38be2bf4e4fec1da96180795032115687b32c7cb5dcf705c44caeafea6a83fc198e2ecedc4ec40849abc9a8bef33a  multidict-4.7.0.tar.gz"
